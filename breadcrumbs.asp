<nav aria-label="complementary" class="tcu-breadcrumbs">
    <div class="tcu-layout-constrain cf">
        <span class="tcu-breadcrumbs__crumb" typeof="v:Breadcrumb"><a class="tcu-icon-home" href="http://www.tcu.edu" title="Texas Christian University"><svg height="18" width="20" focusable="false"><use xlink:href="#home-icon"></use></svg><span class="tcu-visuallyhidden">Texas Christian University</span></a></span>
        <span class="tcu-breadcrumbs__chevron">&#8250;</span> <a href="{{ page.path }}">Page Title</a>
    </div>
</nav>