<!--#include file="header.asp" -->

	<!--#include file="breadcrumbs.asp" -->

		<div class="tcu-layoutwrap--transparent">

			<div class="tcu-layout-constrain cf">

				<main class="unit size2of3 m-size2of3 cf">

					<!--
					* We add #main name anchor to our content element because we have a skip
					* main navigation link for accessibility
					*/
					-->
					<a name="main" tabindex="-1" id="main"><span class="tcu-visuallyhidden">Main Content</span></a>

					<article class="tcu-article cf">

						<div class="tcu-article__content cf" itemprop="articleBody">
							<!--#include file="content.html" -->
						</div>
					</article>

				</main><!-- end of .unit -->

				<aside class="tcu-sidebar unit size1of3 m-size1of3 cf">
					<!--#include file="sidenav.asp" -->
				</aside>

			</div><!-- end of .tcu-layout-constrain -->

		</div><!-- end .tcu-layoutwrap--transparent -->

<!--#include file="footer.asp" -->
