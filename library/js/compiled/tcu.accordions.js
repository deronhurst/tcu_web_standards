'use strict';

/*eslint wrap-iife: [2, "inside"]*/

/**
 * This file adds the accordion functionality. It adds an inactiveClass to all headings.
 * Then it opens the first accordion element by adding an activeClass to it and adding the openClass
 * to it's sibling. We also make sure that we add correct aria- labels for accessiblity support. We add
 * an event listener to all headings so we can open/close the accordion content.
 *
 * @author Mayra Perales <m.j.perales@tcu.edu>
 *
 * @summary Open/Close accordions
 *
 * @since 4.5.12
 */
(function (window) {
    'use strict';

    /**
     *  Main function
     * Sets default options and instatiates accordions.
     *
     * @param {Object} options  List of options to modify accordions
     */

    function tcuAccordions(options) {

        // default options
        var defaults = {
            target: '.tcu-accordion-container',
            accordionHeader: '.tcu-accordion-header',
            accordionContent: '.tcu-accordion-content',
            inactiveClass: 'tcu-inactive-header',
            activeClass: 'tcu-active-header',
            openClass: 'tcu-open-content'
        };

        // Combine defaults with user input
        options = extend(defaults, options);

        // Select accordion element
        var accordion = document.querySelectorAll(options.target);
        var accordionLength = accordion.length;

        // Bail if there's no accordion element
        if (null === accordion || !accordion) {
            return;
        }

        // Our variables for easier use
        var accordionHeader = options.accordionHeader;
        var accordionContent = options.accordionContent;
        var inactiveClass = options.inactiveClass;
        var activeClass = options.activeClass;
        var openClass = options.openClass;
        var firstHeading = flattenArray(accordionHeader + ':first-of-type');
        var firstContent = flattenArray(accordionContent + ':first-of-type');
        var contentElements = flattenArray(accordionContent);
        var allHeadings = flattenArray(accordionHeader);
        var x;

        // instatiate accordions
        init();

        /********************* Function Declarations ************************/

        /**
         * Init function
         *
         * @private
         */
        function init() {

            /**
             * Loop through accordion content and add aria-labels
             */
            for (x = 0; x < contentElements.length; x++) {

                // Add aria labels
                contentElements[x].setAttribute('aria-hidden', 'true');
                contentElements[x].setAttribute('aria-label', 'Accordion Content');
            }

            /**
             * Loop through all headings and add an event click
             */
            for (x = 0; x < allHeadings.length; x++) {

                // add inactive class to headings
                allHeadings[x].classList.add(inactiveClass);

                // Let screen readers know this element has a popup
                allHeadings[x].setAttribute('aria-haspopup', 'true');

                // Add click event to all headings
                allHeadings[x].addEventListener('click', onClickListener);
            }

            // Open first accordion
            for (x = 0; x < firstHeading.length; x++) {
                openAccordion(firstHeading[x], firstContent[x]);
            }
        }

        /**
         * Click event listener on all accordion headings
         *
         * @param {Node} event Click event target
         */
        function onClickListener(event) {
            var target = event.target;
            var sibling = target.nextElementSibling;
            var openElements = target.parentNode.querySelectorAll('.' + activeClass);
            var openElementsCount = openElements.length;

            if (target.classList.contains(inactiveClass)) {
                openAccordion(target, sibling);
            } else if (target.classList.contains(activeClass)) {
                closeAccordion(target, sibling);
            }

            // only open one accordion at a time
            if (0 < openElementsCount) {
                for (x = 0; x < openElementsCount; x++) {
                    closeAccordion(openElements[x], openElements[x].nextElementSibling);
                }
            }
        }

        /**
         * Displays the accordion content by replacing the inactiveClass.
         * It adds the openClass to sibling. This also changes the
         * aria-hidden to false.
         *
         * @param {Node} target  Click event target (should be the heading)
         * @param {Node} sibling Event's sibling (should be the content)
         */
        function openAccordion(target, sibling) {

            // replace class of target
            target.classList.remove(inactiveClass);
            target.classList.add(activeClass);

            // Check if jQuery is added
            if (window.jQuery) {
                jQuery.Velocity(sibling, 'slideDown', 300);
            } else {
                window.Velocity(sibling, 'slideDown', 300);
            }

            // replace class for sibling
            sibling.classList.add(openClass);

            // change aria labels for sibling
            sibling.setAttribute('aria-hidden', 'false');
        }

        /**
         * Hides the accordion content by replacing the activeClass.
         * It removes the openClass from sibling. This also changes the
         * aria-hidden to true.
         *
         * @param {Node} target  Click event target (should be the heading)
         * @param {Node} sibling Event's sibling (should be the content)
         */
        function closeAccordion(target, sibling) {

            // replace class of target
            target.classList.remove(activeClass);
            target.classList.add(inactiveClass);

            // Check if jQuery is added
            if (window.jQuery) {
                jQuery.Velocity(sibling, 'slideUp', 300);
            } else {
                window.Velocity(sibling, 'slideUp', 300);
            }

            // replace class for sibling
            sibling.classList.remove(openClass);

            // change aria labels for sibling
            sibling.setAttribute('aria-hidden', 'true');
        }

        /**
         * Merge defaults with user options
         *
         * @private
         * @param   {Object} defaults Default settings
         * @param   {Object} options  User options
         * @returns {Object} Merged values of defaults and options
         */
        function extend(defaults, options) {
            var extended = {};
            var prop;
            for (prop in defaults) {
                if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
                    extended[prop] = defaults[prop];
                }
            }
            for (prop in options) {
                if (Object.prototype.hasOwnProperty.call(options, prop)) {
                    extended[prop] = options[prop];
                }
            }
            return extended;
        }

        /**
         * Push elements into an array and concat the multi level array.
         * This function loops through the accordion array.
         *
         * @param {String} querySelector String to use within querySelectorAll
         * @private
         * @return {Array} flatArray   One level array for easier access
         */
        function flattenArray(querySelector) {
            var items = [];

            // loop through the menu array and push found elements into items
            for (var x = 0; x < accordionLength; x++) {
                items.push(accordion[x].querySelectorAll(querySelector));
            }

            // let's flatten our flatArray array
            var flatArray = items.reduce(function (array, node) {
                for (var c = 0; c < node.length; c++) {
                    array.push(node[c]);
                }
                return array;
            }, []);

            return flatArray;
        }
    } // end of tcuAccordions();

    // add to global namespace
    window.tcuAccordions = tcuAccordions;
})(window);