<!-- .tcu-main-nav for meanmenu.js -->
<nav aria-label="Primary" class="tcu-main-nav tcu-layoutwrap--purple tcu-pad-t0 tcu-pad-b0">
    <ul class="tcu-layout-constrain tcu-top-nav cf">
        <li><a href="default.asp">Home</a></li>
        <li><a href="page-full-width.asp">Full Width</a></li>
        <li class="menu-item menu-item-has-children"><a href="#">About</a>
            <ul class="sub-menu">
                <li><a href="#">Test</a></li>
                <li><a href="#">Test</a></li>
                <li><a href="#">Test</a></li>
                <li><a href="#">Test</a></li>
            </ul>
        </li>
        <li class="menu-item menu-item-has-children"><a href="page-left-sidebar.asp">Page Left Sidebar</a>
            <ul class="sub-menu">
                <li><a href="#">Test</a></li>
                <li><a href="#">Test</a></li>
                <li><a href="#">Test</a></li>
                <li><a href="#">Test</a></li>
            </ul>
        </li>
    </ul>
</nav>