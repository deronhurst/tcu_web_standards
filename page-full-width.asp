<!--#include file="header.asp" -->

    <!--#include file="breadcrumbs.asp" -->

            <div class="tcu-layoutwrap--transparent">

                <div class="tcu-layout-constrain inner-content cf">

                    <main class="unit size1of1 m-size1of1 tcu-below16 cf">

                        <!--
                        * We add #main name anchor to our content element because we have a skip
                        * main navigation link for accessibility
                        */
                        -->
                        <a name="main" tabindex="-1" id="main"><span class="tcu-visuallyhidden">Main Content</span></a>

                        <article class="tcu-article cf">

                            <div class="tcu-article__content cf" itemprop="articleBody">
                                <!--#include file="content.html" -->
                            </div>
                        </article>

                    </main><!-- end of .unit -->

            </div><!-- end of .tcu-layout-constrain -->

        </div><!-- end .tcu-layoutwrap--transparent -->

<!--#include file="footer.asp" -->
