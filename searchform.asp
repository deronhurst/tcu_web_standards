<form role="search" method="get" class="tcu-searchform cf" action="#" >
    <label class="screen-reader-text tcu-visuallyhidden tcu-searchform__label" for="s">Search for:</label>
    <input class="tcu-searchform__text" type="search" value="" name="s" id="s" placeholder="Search the Site..." />
    <input title="Search" class="tcu-searchform__submit" type="submit" value="Go">
</form>