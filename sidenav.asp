<div class="tcu-sidebar__widget widget_nav_menu cf">
    <h4 class="tcu-sidebar__widget-title">Web Standards</h4>
    <div>
        <ul class="menu">
            <li><a href="#">Getting Started</a></li>
            <li class="menu-item-has-children">
                <a href="#">WordPress Policies</a>
                    <ul class="sub-menu">
                        <li><a href="#">Plugins</a></li>
                        <li><a href="#">Database Migration</a>
                        <li class="menu-item-has-children">
                            <a href="#">WordPress Policies</a>
                                <ul class="sub-menu">
                                    <li><a href="#">Plugins</a></li>
                                    <li><a href="#">Database Migration</a></li>
                                </ul>
                        </li>
                        <li><a href="#">Website Validation</a></li>
                    </ul>
                </li>
            <li><a href="#">Compliance &amp; Accessibility</a></li>
        </ul>
    </div>
</div>

<div class="tcu-sidebar__widget widget_nav_menu cf">
    <h4 class="tcu-sidebar__widget-title">Second Nav</h4>
    <div>
        <ul class="menu">
            <li><a href="#">Getting Started</a></li>
            <li class="menu-item-has-children">
                <a href="#">WordPress Policies</a>
                    <ul class="sub-menu">
                        <li><a href="#">Plugins</a></li>
                        <li><a href="#">Database Migration</a>
                        <li class="menu-item-has-children">
                            <a href="#">WordPress Policies</a>
                                <ul class="sub-menu">
                                    <li><a href="#">Plugins</a></li>
                                    <li><a href="#">Database Migration</a></li>
                                </ul>
                        </li>
                        <li><a href="#">Website Validation</a></li>
                    </ul>
                </li>
            <li><a href="#">Compliance &amp; Accessibility</a></li>
        </ul>
    </div>
</div>