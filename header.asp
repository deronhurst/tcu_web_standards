<%@LANGUAGE="JAVASCRIPT" CODEPAGE="65001"%>
<!doctype html>
<!--[if lt IE 7]><html lang="en-US" class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html lang="en-US" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html lang="en-US" class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-US" class="no-js"><!--<![endif]-->

    <head>
        <meta charset="utf-8">

        <!-- force Internet Explorer to use the latest rendering engine available -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>TCU | Static Site</title>

        <!-- mobile meta -->
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <!-- icons & favicons -->
        <link rel="apple-touch-icon" href="/library/images/apple-icon-touch.png">
        <link rel="icon" href="/favicon.png">
        <!--[if IE]>
            <link rel="shortcut icon" href="/favicon.ico">
        <![endif]-->
        <!-- favicon.ico for IE10 win -->
        <meta name="msapplication-TileColor" content="#4d1979">
        <meta name="msapplication-TileImage" content="/library/images/win8-tile-icon.png">

        <!-- Modernizr  -->
        <script src="library/js/libs/modernizr.custom.min.js"></script>

        <link rel="stylesheet" href="library/css/style.min.css" type="text/css" media="all">

    </head>

    <body>

        <div class="tcu-layout-container">

            <header class="tcu-header cf" role="banner">

                <!-- Our SVG Library - Do not remove! -->
                <!--#include file="sprite.symbol.svg" -->

                <!--
                /**
                * Make sure to have a container with the #main ID
                * include it in the main content
                */
                -->
                <a href="#main" class="tcu-skip-nav tcu-visuallyhidden">Skip to main content</a>

                <div class="tcu-layoutwrap--dark tcu-layoutwrap--dark--border tcu-not-mobile">
                    <!--#include file="universalnav.asp" -->
                </div>

                <div class="tcu-layout-constrain cf">
                    <a class="tcu-banner__logo" href="http://www.tcu.edu/" rel="nofollow">
                        <img height="113" width="129" src="library/images/svg/tcu-logo-banner.svg" alt="Texas Christian University logo">
                        <span class="tcu-visuallyhidden">Texas Christian University</span>
                    </a>
                    <div class="tcu-header__site-title"><a aria-label="Go back to the homepage" href="/index.html">TCU | Static Site</a></div>
                </div>

                <!--#include file="mainnav.asp" -->

            </header><!-- end of .tcu-header -->
