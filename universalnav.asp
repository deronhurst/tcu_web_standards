<!-- .nav-universal__is-mobile for meanmenu.js -->
<ul class="tcu-layout-constrain tcu-nav-universal tcu-nav-universal__is-mobile cf">
    <li class="tcu-nav-universal__icons">
        <a href="http://www.tcu.edu/a_to_z.asp"><svg focusable="false" width="15" height="25"><use xlink:href="#list-icon"></use></svg>
            A-Z directory
        </a>
    </li>
    <li class="tcu-nav-universal__icons">
        <a href="http://www.makeagift.tcu.edu"><svg focusable="false" width="15" height="25"><use xlink:href="#gift-icon"></use></svg>
            Make a gift
        </a>
    </li>
    <li class="tcu-nav-universal__icons tcu-has-children menu-item-has-children">
        <a href="#"><svg focusable="false" width="15" height="25"><use xlink:href="#person-icon"></use></svg>
            I'm looking for...
        </a>
        <ul class="tcu-dropdown tcu-dropdown--grey tcu-unstyled-list sub-menu cf">
            <li><a href="http://admissions.tcu.edu/">Apply</a></li>
            <li><a href="http://www.admissions.tcu.edu/Visit-TCU">Visit</a></li>
            <li><a href="https://my.tcu.edu">MyTCU</a></li>
            <li><a href="https://mail.tcu.edu">My TCU Email</a></li>
        </ul>
    </li>
    <li class="tcu-search-form__mobile" role="search">
    <!--#include file="searchform.asp" -->
    </li>
</ul>
