        <footer class="tcu-footer cf">
            <div class="tcu-footer-departments">
                <div class="tcu-layout-constrain tcu-footer-departments__widgets line below24 cf">

                    <!--#include file="wordmark.asp" -->

                    <!--#include file="contactinfo1.asp" -->

                    <!--#include file="contactinfo2.asp" -->
                </div>
            </div> <!-- end of .tcu-footer-departments -->

            <div class="tcu-footer-main cf">

                <div class="tcu-footer__fountain"></div>

                <div class="tcu-footer__inner cf">

                    <a class="tcu-logo tcu-logo--white" href="http://www.tcu.edu/">
                        <img height="70" width="143" src="library/images/svg/tcu-logo.svg" alt="Texas Christian University logo"><span class="tcu-visuallyhidden">Texas Christian University</span>
                    </a>

                    <p class="tcu-layout-constrain tcu-footer__inner__links">
                        <a href="http://www.maps.tcu.edu/">Maps &amp; Directions</a>
                    </p>

                    <div class="tcu-layout-constrain tcu-footer__inner__text" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <p><span itemprop="streetAddress">2900 South University Drive<br>
                            Fort Worth, Texas 76129 <br></span>
                        <span itemprop="telephone">Ph 817-257-7000</span></p>
                    </div>

                    <nav aria-label="Secondary" class="tcu-layoutwrap--dark">
                        <ul class="tcu-footer__inner__wrap__nav cf">
                            <li><a title="Accessibility" href="https://accessibility.tcu.edu/">Accessibility</a></li>
                            <li><span> / </span><a title="Employment" href="http://www.hr.tcu.edu/work-at-tcu">Employment</a></li>
                            <li><span> / </span><a title="Legal disclosures" href="http://www.hea101.tcu.edu">Legal disclosures</a></li>
                            <li><span> / </span><a title="Notice of non-discrimination" href="http://www.tcu.edu/notice-of-nondiscrimination.asp">Notice of non-discrimination</a></li>
                            <li><span> / </span><a title="Title IX" href="http://www.titleix.tcu.edu">Title IX</a></li>
                        </ul>
                    </nav>

                </div><!-- end of .tcu-footer__inner -->

                <div class="tcu-footer__inner__copyright cf">

                    <div class="group unit size1of1 m-size1of1">
                        <div class="tcu-socialmedia unit m-size1of2 size1of2">

                            <a href="https://www.facebook.com/pages/Fort-Worth-TX/TCU-Texas-Christian-University/155151195064" title="The official TCU's Facebook account"><svg focusable="false" height="30" width="30"><use xlink:href="#facebook"></use></svg><span class="tcu-visuallyhidden">Facebook</span></a>

                            <a href="https://www.flickr.com/photos/texaschristianuniversity/" title="The official TCU's  Flickr account"><svg focusable="false" height="30" width="30"><use xlink:href="#flickr"></use></svg><span class="tcu-visuallyhidden">Flickr</span></a>

                            <a href="https://www.instagram.com/texaschristianuniversity/" title="The official TCU's  Instagram account"><svg focusable="false" height="30" width="30"><use xlink:href="#instagram"></use></svg><span class="tcu-visuallyhidden">Instagram</span></a>

                            <a href="https://www.pinterest.com/tcuedu/" title="The official TCU's Pinterest account"><svg focusable="false" height="30" width="30"><use xlink:href="#pinterest"></use></svg><span class="tcu-visuallyhidden">Pinterest</span></a>

                            <a href="<?php echo get_template_directory_uri(); ?>/library/images/snapcode.jpeg" title="The official TCU's Snapchat account"><svg focusable="false" height="30" width="30"><use xlink:href="#snapchat"></use></svg><span class="tcu-visuallyhidden">Snapchat</span></a>

                            <a href="https://twitter.com/tcu" title="The official TCU's Twitter account"><svg focusable="false" height="30" width="30"><use xlink:href="#twitter"></use></svg><span class="tcu-visuallyhidden">Twitter</span></a>

                            <a href="https://www.youtube.com/user/TCU" title="The official TCU's YouTube account"><svg focusable="false" height="30" width="30"><use xlink:href="#youtube"></use></svg><span class="tcu-visuallyhidden">YouTube</span></a>

                            <span> / <a title="TCU's official social media directory" href="http://www.mkc.tcu.edu/social-directory.asp">Social Media Directory</a></span>
                        </div>

                        <div class="unit m-size1of2 size1of2">
                            Copyright &copy; <script>
                                                var theYear = new Date(); document.write(theYear.getFullYear());
                                            </script> <a aria-label="Go back to the main www.tcu.edu homepage" href="http://www.tcu.edu">Texas Christian University</a>. All rights reserved.
                        </div>
                    </div><!-- end of .group -->

                    <div class="group tcu-accrediation unit size1of1 m-size1of1 tcu-below16 tcu-top16">
                        <p class="unit size1of2 m-size1o1"></p>
                        <p class="unit size1of2 m-size1of1 tcu-small tcu-mar-tb0">Texas Christian University is accredited by the Commission on Colleges of the Southern Association of Colleges and Schools to award baccalaureate, masters, doctoral degrees. Contact the Commission on Colleges at 1866 Southern Lane, Decatur, Georgia 30033-4097 or call 404-679-4500 for questions about the accreditation of Texas Christian University.</p>
                    </div>
                </div><!-- end of .tcu-footer__inner__copyright -->

                 <!-- Scroll to top -->
                <button type="button" class="tcu-top" title="Back to top"><span class="tcu-visuallyhidden">Top</span></button>

            </div><!-- end of .tcu-footer-main -->

        </footer><!-- end of .tcu-footer -->

    </div><!-- end of .tcu-layout-container -->

    <!-- all js scripts  -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="library/js/libs/velocity.min.js"></script>
    <script type="text/javascript" src="library/js/min/jquery.meanmenu.min.js"></script>
    <script type="text/javascript" src="library/js/min/tcu.menu.min.js"></script>
    <script type="text/javascript" src="library/js/libs/jquery.responsiveTabs.min.js"></script>
    <script type="text/javascript" src="library/js/min/tcu.accordions.min.js"></script>
    <script type="text/javascript" src="library/js/min/scripts.min.js"></script>
    </body>

</html>

